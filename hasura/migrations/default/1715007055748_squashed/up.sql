

comment on column "public"."profiles"."pubkey" is E'ss58 address of profile owner';

CREATE TABLE "public"."transaction_comments" ("index_request_cid" text NOT NULL, "pubkey" text NOT NULL, "tx_id" text NOT NULL, "comment" text NOT NULL, PRIMARY KEY ("tx_id","pubkey") , UNIQUE ("index_request_cid"));COMMENT ON TABLE "public"."transaction_comments" IS E'Transaction comments';

comment on column "public"."transaction_comments"."pubkey" is E'ss58 address of author';

comment on column "public"."transaction_comments"."tx_id" is E'transaction id in the form "blockNumber-hashStart-eventNumber"';

comment on column "public"."transaction_comments"."comment" is E'content of the transaction comment';

alter table "public"."transaction_comments" add column "time" timestamptz
 not null;

ALTER TABLE "public"."profiles" ALTER COLUMN "time" TYPE timestamp;

ALTER TABLE "public"."transaction_comments" ALTER COLUMN "time" TYPE timestamp;
comment on column "public"."transaction_comments"."time" is E'timestamp of the index request';
