import { timestampToKey } from '../processor'

console.log(timestampToKey(1519405679000)) // 00000161c3a2c198
console.log(timestampToKey(1519405679)) // 000000005a904a6f
console.log(timestampToKey(1523008319)) // 000000005ac7433f
console.log(timestampToKey(1625151291)) // 0000000060ddd73b
console.log(timestampToKey(1625151291000)) // 0000017a6290be78

console.log(timestampToKey(1725985291005)) // 00000191dcbd7afd

console.log(timestampToKey(1539382746000)) // 000001666a5c9b90
