// This script shows a minimal working example to publish a datapod profile

// Explaning dependencies:
// - consts: contains common kinds for index requests
// - types: contains details about index request fields
// - kubo-rpc-client: simple rpc client lib available in many langages
// - collector: contains function defining expected payload to sign
// - polkadot: contains crypto utils

import { readFileSync } from 'fs'
import { CESIUM_PLUS_PROFILE_INSERT } from '../consts'
import type { IndexRequest } from '../types'
import { create } from 'kubo-rpc-client'
import type { ImportCandidate, KuboRPCClient } from 'kubo-rpc-client'
import { buildStringPayload } from '../collector'
import { Keyring } from '@polkadot/keyring'
import { u8aToHex } from '@polkadot/util'

// This public Kubo node exposes features necessary to upload data and relay to datapod network
// This is a simple alternative to embedding an ipfs node when it is not possible or suited
// Bear in mind that the data sent here can be garbage collected at any moment.
// If the datapod network did not pin it, it will simply disappear.
// The datapod network currently does not have pinning strategy and pins nothing.
const KUBO_RPC = 'https://rpc.datapod.gyroi.de/'

const kubo: KuboRPCClient = create({
  url: new URL(KUBO_RPC)
})

// main function describing the steps of datapod publication process
async function main() {
  // 0. keyring
  const keyring = new Keyring({ type: 'ed25519', ss58Format: 42 })
  const alice = keyring.addFromUri('//Alice')

  // 1. profile picture
  // simply get image as buffer for demo
  const img = getImage()
  // upload the image and get its CID
  const img_cid = (await kubo.add(img)).cid

  // 2. profile
  // build the profile
  const data = { title: 'Alice Example', description: 'This is a demo', avatar: img_cid }
  // upload the profile and get its CID
  const data_cid = await kubo.dag.put(data)

  // 3. partial index request
  // build the index request
  const ir: IndexRequest = {
    pubkey: alice.address,
    time: Date.now(),
    kind: CESIUM_PLUS_PROFILE_INSERT,
    data: data_cid,
    sig: null
  }

  // 4. signature
  // define the binary payload
  const payload = buildStringPayload(ir)
  // sign the payload
  const signature = alice.sign(payload)
  // put signature in partial index request
  ir.sig = u8aToHex(signature)

  // 5. index request
  // upload index request
  const ir_cid = await kubo.dag.put(ir)

  // 6. broadcast index request CID on pubsub
  const enc = new TextEncoder()
  await kubo.pubsub.publish('ddd', enc.encode(ir_cid.toString() + '\n'))

  // some logging when you run it
  console.log(`➡️ published ${ir_cid} on pubsub`)
  console.log('➡️ corresponding to the following index request')
  console.log(ir)
  console.log('➡️ that has the given profile')
  console.log(data)
  console.log('➡️ you can visit the following link to confirm that data has been indexed')
  console.log('https://duniter--vue-coinduf-eu.ipns.pagu.re/#/data/' + alice.address)
}

// run the main function
main()

// get image for demo purpose
function getImage(): ImportCandidate {
  const buffer = readFileSync('logo.png')
  const bytearray = new Uint8Array(buffer)
  return { content: bytearray }
}
