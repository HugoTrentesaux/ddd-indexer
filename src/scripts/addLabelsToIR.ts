import { kubo } from '../kubo'
import { timestampToKey } from '../processor'
import { CID } from 'multiformats'
import { appendFile, createReadStream } from 'fs'
import { createInterface } from 'readline'

async function addLabels(input: string, output: string) {
  const LIMIT = 500 // max number of lines to process simultaneously
  const NOTIF = 2000 // log every N lines processed
  const rejected = './input/HS.txt'
  let queueSize = 0
  let readTotal = 0

  function process(line: string) {
    queueSize++
    if (queueSize > LIMIT) {
      linereader.pause()
    }
    try {
      const irCid = line
      const irCID = CID.parse(irCid)
      kubo.dag
        .get(irCID)
        .then((x) => timestampToKey(x.value.time) + ' ' + irCid + '\n')
        .then((l) =>
          appendFile(output, l, () => {
            readTotal++
            queueSize--
            if (queueSize < LIMIT) {
              linereader.resume()
            }
            if (readTotal % NOTIF == 0) {
              console.log(`processed ${readTotal} profiles`)
            }
          })
        )
        .catch((e) => {
          console.log(e)
          appendFile(rejected, line, () => {
            readTotal++
          })
        })
    } catch (e) {
      console.log(e)
      appendFile(rejected, line + '\n\n\n', () => {})
    }
  }
  const linereader = createInterface(createReadStream(input))
  linereader.on('line', process)
  linereader.on('close', () => console.log('done'))
}

// addLabels('./input/cids.txt', './input/cids+labels.txt')
addLabels('./input/devIr.txt', './input/devIr+labels.txt')
