import { uniqueby } from '../utils'
import { CID } from 'multiformats'

let a = [
  CID.parse('bafyreicvlp2p65agkxpzcboedba7zit55us4zvtyyq2wesvsdedy6irwfy'),
  CID.parse('bafyreicvlp2p65agkxpzcboedba7zit55us4zvtyyq2wesvsdedy6irwfy')
]

let s = new Set(a)

console.log(s)
// two elements

let u = uniqueby(a, (e) => e.toString())

console.log(u)
// one element