import { create } from 'kubo-rpc-client'
import type { KuboRPCClient } from 'kubo-rpc-client'
import { Agent } from 'http'
import { EMPTY_NODE_CID } from '../consts'

// this script demonstrate the use of a subset of the API served through nginx
// allows to use a remote node without admin privilege on it

const KUBO_RPC = 'https://rpc.datapod.gyroi.de/'

const kubo: KuboRPCClient = create({
  url: new URL(KUBO_RPC),
  agent: new Agent({
    maxSockets: 50000
  })
})

async function doit() {
  // getting works
  const n = (await kubo.dag.get(EMPTY_NODE_CID)).value
  console.log(n)
  // putting works
  const obj = { kind: 123456, value: "this is a text I'm going to publish", something: [1, 2, 3] }
  const cid = await kubo.dag.put(obj)
  console.log(cid)
  // pubsub publish works
  const enc = new TextEncoder()
  await kubo.pubsub.publish('ddd', enc.encode("hello, I'm there\n"))
  console.log('published ;)')
  // makes sure that pinning does not work
  await kubo.dag.put(obj, { pin: true })
}

doit()
