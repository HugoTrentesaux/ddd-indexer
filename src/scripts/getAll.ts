import { kubo } from '../kubo'
import { getAll } from '../interface'
import { CID } from 'multiformats'
import type { IndexRequest } from '../types'
import { CESIUM_PLUS_PROFILE_IMPORT, CESIUM_PLUS_PROFILE_INSERT } from '../consts'

const LOG_EVERY = 1000

const cid = CID.parse('bafyreidnn24efeospk67qbaj7wipug3ewkqzleniczt4tsdsevplz5ynwe')

const iterator = getAll(cid)

console.log('in', cid)

async function doit() {
  let num = 0
  for await (const item of iterator) {
    for (let irCID of item.leaf) {
      num += 1
      kubo.dag
        .get(irCID)
        .then((res) => res.value)
        .then((r) => {
          const ir = r as IndexRequest
          if (ir.kind != CESIUM_PLUS_PROFILE_IMPORT) {
            console.log(item.key, irCID.toString())
          }
        })
      if (num % LOG_EVERY == 0) {
        console.log(num)
      }
    }
  }
  console.log(num)
}

doit()

// iterate over all nodes and show the ones that are not cesium plus imports