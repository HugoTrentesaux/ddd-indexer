// vue app dependencies
import './assets/main.css'
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { feed } from './global'

// collector dependencies
import { TOPIC } from './consts'
import { CID } from 'multiformats'
import { kubo } from './kubo'
import type { IndexRequest } from './types'
import { buildStringPayload, isValidSignature } from './collector'

// activate this for debug purpose if you want to do indexing directly from the browser app
// instead of the nodejs proces
// import { addToIndex } from './processor'

// === define what to do of pubsub messages and start listening ===

// message handler used for demo purpose when in app mode, to display results in the feed
function handleMessage(message: any) {
  const msg = new TextDecoder().decode(message.data).trim()
  try {
    const cid = CID.parse(msg)
    feed.value.push(msg)
    kubo.dag.get(cid).then(function (d) {
      const dag = d.value as IndexRequest
      const stringPayload = buildStringPayload(dag)
      const isValid = isValidSignature(stringPayload, dag.sig!, dag.pubkey)
      if (isValid) {
        // here we would do the processing
        // addToIndex(cid, dag)
      } else {
        feed.value.push('[invalid sig] ' + msg)
      }
    })
  } catch {
    feed.value.push('[invalid] ' + msg)
  }
}
// subscribe
kubo.pubsub.subscribe(TOPIC, handleMessage)

// === APP UI ===
const app = createApp(App)
app.use(router)
app.mount('#app')
