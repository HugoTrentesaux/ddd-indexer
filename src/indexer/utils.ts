import { EMPTY_NODE_CID } from '../consts'
import { CID } from 'multiformats'
import { kubo } from '../kubo'
import { ddKeys } from './ipns'
import { getLatestIndexedCID } from './database'
import { emptyRootInode } from '../types'

// get root cid from arg if given
// initialize it:
// - from CID if given
// - from IPNS if given
// - from default IPNS else
// - as empty node else
export async function getRootCIDfromArgs(argv: string[]): Promise<CID> {
  // --- using arg allows to overwrite startup CID ---
  if (argv.length >= 3) {
    // expects /ipfs/<CID> or /IPNS/<KEY>
    const arg = process.argv[2]
    // 1. user is giving a CID
    if (arg.startsWith('/ipfs/')) {
      try {
        const rootCID = CID.parse(arg.substring(6))
        console.log(`🔨 using given ${rootCID} as startup root node`)
        return rootCID
      } catch {
        console.log(`can not parse ${arg} as CID`)
        process.exit()
      }
    }
    // 2. user is giving an IPNS (must point to a CID)
    if (arg.startsWith('/ipns/')) {
      try {
        for await (const name of kubo.name.resolve(arg, { nocache: true })) {
          const cid = CID.parse(name.substring(6))
          console.log(`🔨 using resolved ${cid} as startup root node`)
          return cid
        }
      } catch {
        console.log(`${arg} does not point to a CID`)
        process.exit()
      }
    }
  }
  // --- no arg given, using default ---
  // 1. reading from self-published tree
  const self_bootstrap = ddKeys.tamt
  try {
    // timeout because resolving local ipns
    for await (const name of kubo.name.resolve(self_bootstrap, { timeout: 1000, nocache: true })) {
      const cid = CID.parse(name.slice(6))
      console.log(`🔨 using ${cid} as startup root node`)
      console.log(`   resolved from self ${self_bootstrap}`)
      return cid
    }
  } catch {
    console.log('🐞', self_bootstrap, 'does not resolve to a CID')
  }
  // 2. if this failed, reads from db (for example if ipfs node was reinitialized but db persisted)
  const latestCID = await getLatestIndexedCID()
  if (latestCID) {
    console.log(`🔨 using ${latestCID} as startup root node`)
    console.log(`   resolved from self database`)
    return latestCID
  }
  // 3. else, starts from scratch, data will be imported from peers later on
  console.log(`🔨 starting from scratch: ${EMPTY_NODE_CID}`)
  console.log('🌱 adding empty node to ipfs')
  const node = emptyRootInode()
  await kubo.dag.put(node, { pin: true })
  return EMPTY_NODE_CID
}
