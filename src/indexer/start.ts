import { EMPTY_NODE_CID, TOPIC } from '../consts'
import { resolveHist } from '../processor'
import { getPubSubHandler } from '../collector'
import { KUBO_RPC, kubo, kubo2 } from '../kubo'
import type { IndexHist } from '../types'
import { CID } from 'multiformats'
import { computeDiff, GLOB_root } from './handlers'
import { takeFromDiffQueue, takeFromProcessQueue, takeFromMergeQueue, validMessageHandler } from './handlers'
import { events, evtype } from './handlers'
import type { DdKeys } from './types'
import { getRootCIDfromArgs } from './utils'
import { DD_TAMT_HIST_OPT, ddKeys } from './ipns'
import { initHistIfNull, publishKeys, TRUSTED_PEERS } from './bootstrap'
import { getLatestIndexedCID, setLatestIndexedCID } from './database'

// ----- regularly publish history
function periodicHistPublish(interval: number) {
  setInterval(histPublish, interval)
}
async function histPublish(): Promise<void> {
  // if history is up to date, to nothing
  if (GLOB_hist.current_index.toString() == GLOB_root.cid.toString()) return
  // else, update the history
  const newHist: IndexHist = {
    last_history: GLOB_histCID,
    current_index: GLOB_root.cid,
    number: GLOB_hist.number + 1,
    timestamp: Date.now()
  }
  const newHistCID = await kubo.dag.put(newHist)
  kubo.name.publish(newHistCID, DD_TAMT_HIST_OPT)
  // update global vars
  GLOB_hist = newHist
  GLOB_histCID = newHistCID
  return
}

// ----- regularly synchronize from peers
function periodicPeerSync(interval: number) {
  setInterval(async () => peerSync(TRUSTED_PEERS), interval)
}
async function peerSync(trusted_peer_list: string[]): Promise<void> {
  for (const peer of trusted_peer_list) {
    console.log('🔄 syncing from peer', peer)
    try {
      // resolve peer root keys
      for await (const name of kubo.name.resolve(peer)) {
        const cid = CID.parse(name.slice(6))
        const peerDdKeys: DdKeys = (await kubo.dag.get(cid)).value
        // resolve tamt key
        for await (const name of kubo.name.resolve(peerDdKeys.tamt)) {
          const cid = CID.parse(name.slice(6))
          // found peer tree, request index diff
          events.emit(evtype.triggerComputeDiff, GLOB_root.cid, cid)
        }
      }
    } catch (e) {
      console.error('❌ could not find index of trusted peer', peer, 'due to', e)
    }
  }
  return
}

// pubsub configuration
// subscribe options
const pubsubSubscribeOptions = {
  onError: pubsubErrorCallback
  // signal: signal
  // timeout: 1000
}
// error handling in pubsub subscription: log the error and re-subscribe
function pubsubErrorCallback(e: any) {
  console.log('+++ pubsub error +++')
  anyErrorCallback(e)
}
function pubsubAbortCallback(e: any) {
  console.log('+++ pubsub abort +++')
  anyErrorCallback(e)
}
function anyErrorCallback(e: any) {
  console.log(Date.now())
  // console.log(e)
  kubo2.pubsub.unsubscribe(TOPIC)
  pubsubSubscribe()
}
function pubsubSubscribe() {
  kubo2.pubsub
    .subscribe(TOPIC, getPubSubHandler(validMessageHandler), pubsubSubscribeOptions)
    .catch(pubsubAbortCallback)
  console.log('🔌 connected to', KUBO_RPC)
  console.log('👂 listening on topic', TOPIC)
}

// ===================== START ===========================

console.log('✨ starting')
// first thing is to make sure the keys in use are well published
publishKeys()
console.log('🌴 self root key is', ddKeys.root)

// === CONFIG ===
const SECOND = 1000 // 1 second
const MINUTE = 60 * SECOND // 1 minute
const HOUR = 60 * MINUTE // 1 hour
const DAY = 24 * HOUR // 1 day
const _ = DAY // ignore unused
const HIST_PUBLISH_PERIOD = 10 * MINUTE
// regularly sync form peers to compensate from network outage
const PEERSYNC_PERIOD = 10 * MINUTE

// init globals
// set global rootCID from CLI args
GLOB_root.cid = await getRootCIDfromArgs(process.argv)
await initHistIfNull(GLOB_root.cid) // make sure history is available until then
let GLOB_histCID: CID = await resolveHist()
let GLOB_hist: IndexHist = (await kubo.dag.get(GLOB_histCID)).value
histPublish() // publish history to track new start root

// get latest db CID
let latestCID = await getLatestIndexedCID()
if (latestCID == null) {
  latestCID = EMPTY_NODE_CID
  await setLatestIndexedCID(latestCID)
}
console.log(`🛢 latest indexed cid ${latestCID}`)

// bind event handlers
events.on(evtype.triggerProcess, takeFromProcessQueue)
events.on(evtype.triggerIndex, takeFromDiffQueue)
events.on(evtype.triggerComputeDiff, computeDiff)
events.on(evtype.triggerCollect, takeFromMergeQueue)
pubsubSubscribe() // subscribe to index requests channel
periodicHistPublish(HIST_PUBLISH_PERIOD) // regularly publish history
periodicPeerSync(PEERSYNC_PERIOD) // regularly sync from peers

// emit event to tell indexer to start indexing to database
// if it is starting from scratch (emtpy database), it will iterate over all values
// if it already indexed up to a given cid (last_indexed_cid in db), it will only iterate over the diff
events.emit(evtype.triggerComputeDiff, latestCID, GLOB_root.cid)
// at startup browse peer list
peerSync(TRUSTED_PEERS)

// process loop
setInterval(() => {}, 1 << 30)
;['SIGINT', 'SIGTERM', 'SIGQUIT'].forEach((signal) => process.on(signal, () => process.exit()))
