import { CID } from 'multiformats'
import type { IndexRequest } from '../types'

// data added between two CIDs
export interface DiffData {
  oldCID: CID
  newCID: CID
  newItems: Array<[CID, IndexRequest]>
}

// this object allows to share a single ipns key with other datapods
// so that they use components as bootstrap
export interface DdKeys {
  // root entry mapping to all the other ones below
  root: string
  // timestamp AMT, oplog for database storage
  tamt: string
  // history of the TAMT
  tamt_hist: string
  // C+ profiles kinds grouped in a single tree for efficient indexing
  profiles: string
}
