// TODO add dependency for that
export function formatDate(d: Date) {
  return (
    d.getFullYear() +
    '-' +
    (d.getMonth() + 1).toString().padStart(2, '0') +
    '-' +
    d.getDate().toString().padStart(2, '0') +
    ' ' +
    d.getHours().toString().padStart(2, '0') +
    ':' +
    d.getMinutes().toString().padStart(2, '0') +
    ':' +
    d.getSeconds().toString().padStart(2, '0')
  )
}

export function uniqueby<T>(a: Array<T>, k: (e: T) => string) {
  const seen = new Map<string, boolean>()
  const unique = []
  for (const e of a) {
    const key = k(e)
    if (!seen.get(key)) {
      seen.set(key, true)
      unique.push(e)
    }
  }
  return unique
}
