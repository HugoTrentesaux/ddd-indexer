# Edit database

This is how to change something to the database structure:

```sh
# start hasura console that tracks database changes
pnpm hasura console
# do your stuff graphically...
# squash the changes for a cleaner commit history
pnpm hasura migrate squash --from 1712826828679
```
