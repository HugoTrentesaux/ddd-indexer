## Start collector and indexer

You first need to start a kubo node and postgres/hasura.
Then start pubsub collector and database indexer with:

```sh
# start the collector and indexer
pnpm exec tsx src/indexer/start.ts
```
