# Install

To install a datapod, use the [`docker-compose.prod.yml`](../docker-compose.prod.yml) and [`.env.prod.example`](../.env.prod.example) files:

```sh
# edit env file
vim .env
# start services
docker compose up -d
# follow only datapod logs
docker compose logs -f datapod
```

The logs should give you:

```
# TODO
```

## Setup reverse proxy

Depending on what you want to expose publicly, use the following nginx config files examples.

- to expose graphql endpoint, see [`hasura.nginx.conf`](./nginx/hasura.nginx.conf)
- to expose ipfs http gateway, see [`ipfs-gateway.nginx.conf`](./nginx/ipfs-gateway.nginx.conf)
- to expose http gateway with subdomain, see [`subdomain-ipfs-gateway.nginx.conf`](./nginx/subdomain-ipfs-gateway.nginx.conf) (requires wildcard certificates)
- to expose rpc api, see [`ipfs-rpc.nginx.conf`](./nginx/ipfs-rpc.nginx.conf) (should not be done on main IPFS node)

A bit of explanation about what they are:

**graphql endpoint** Endpoint used for search capabilities.  
**ipfs http gateway** A way to get IPFS content (image for example) through http (without embeding a node).  
**subdomain gateway** Same as gateway, but provides origin isolation and root domain, for example for websites.  
**rpc api** A way to "lend" an IPFS node to clients who do not embed one through a subset of the rpc API. This should be done on a different node than the one used by the datapod. It should also come with a rate limiter or some kind of protection mecanism.