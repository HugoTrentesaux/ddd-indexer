Migrate ipfs repo after upgrade

```sh
docker run -it -v datapod_kubo_data:/data/ipfs -v ./emptydir:/container-init.d h30x/datapod-kubo:latest daemon --migrate=true
```