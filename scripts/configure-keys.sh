#!/bin/sh
set -ex

# --- keys ---
# generate key for index history if not defined
ipfs key gen dd_root || true
ipfs key gen dd_tamt || true
ipfs key gen dd_tamt_hist || true
ipfs key gen dd_profiles || true
