#!/bin/sh
set -ex

# --- rpc ---
ipfs config API.HTTPHeaders.Access-Control-Allow-Origin --json '["*"]'
ipfs config API.HTTPHeaders.Access-Control-Allow-Methods --json '["PUT", "POST"]'
