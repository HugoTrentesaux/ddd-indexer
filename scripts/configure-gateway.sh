#!/bin/sh
set -ex

# --- gateway ---
# prevent gateway from fetching foreign data
# ipfs config Gateway.NoFetch --json true
# ipfs config Gateway.NoFetch --json false
# public gateway without subdomain (no wildcard)
# enables /ipfs and /routing (delegated routing)
# public gateway with subdomain (needs wildcard)
ipfs config Gateway.PublicGateways --json "{
  \"$KUBO_GATEWAY_DOMAIN\": { \"UseSubdomains\": false, \"Paths\": [\"/ipfs\", \"/routing\"] },
  \"$KUBO_GATEWAY_SUBDOMAIN\": { \"UseSubdomains\": true, \"Paths\": [\"/ipfs\", \"/ipns\"] }
}"
ipfs config Gateway.ExposeRoutingAPI --json true

# only reprovide pinned data
# ipfs config Reprovider.Strategy "pinned"
# ipfs config Reprovider.Strategy --json null
