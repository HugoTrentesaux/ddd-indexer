#!/bin/sh
set -ex

# --- addresses ---
# enable p2p, quic, webtransport, webrtc 
# ipfs config Swarm.Transports.Network.Websocket --json true
# internal port is always 4001
ipfs config Addresses.Swarm --json '[
  "/ip4/0.0.0.0/tcp/4001",
  "/ip6/::/tcp/4001",
  "/ip4/0.0.0.0/udp/4001/quic-v1",
  "/ip6/::/udp/4001/quic-v1"
]'
# configure the addresses to announce
# KUBO_PORT is external port mapped in docker compose to 4001
ipfs config Addresses.Announce --json "[
    \"/dns/$KUBO_DOMAIN/tcp/$KUBO_PORT\",
    \"/dns/$KUBO_DOMAIN/udp/$KUBO_PORT/quic-v1\"
]"
