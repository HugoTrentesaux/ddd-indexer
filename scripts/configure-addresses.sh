#!/bin/sh
set -ex

# --- addresses ---
# enable p2p, quic, webtransport, webrtc 
# ipfs config Swarm.Transports.Network.Websocket --json true
# internal port is always 4001
ipfs config Addresses.Swarm --json '[
  "/ip4/0.0.0.0/tcp/4001",
  "/ip6/::/tcp/4001",
  "/ip4/0.0.0.0/udp/4001/quic-v1",
  "/ip6/::/udp/4001/quic-v1",
  "/ip4/0.0.0.0/udp/4001/quic-v1/webtransport",
  "/ip6/::/udp/4001/quic-v1/webtransport",
  "/ip4/0.0.0.0/udp/4001/webrtc-direct",
  "/ip6/::/udp/4001/webrtc-direct",
  "/ip4/0.0.0.0/tcp/4002/ws",
  "/ip6/::/tcp/4002/ws"
]'
# configure the addresses to announce
# KUBO_PORT is external port mapped in docker compose to 4001
ipfs config Addresses.Announce --json "[
    \"/dns/$KUBO_DOMAIN/tcp/$KUBO_PORT\",
    \"/dns/$KUBO_DOMAIN/udp/$KUBO_PORT/quic-v1\",
    \"/dns/$KUBO_DOMAIN/udp/$KUBO_PORT/quic-v1/webtransport\",
    \"/dns/$KUBO_DOMAIN/udp/$KUBO_PORT/webrtc-direct\",
    \"/dns/$KUBO_WEBSOCKET_DOMAIN/tcp/443/wss/\"
]"

# --- swarm ---
# disable p2p-circuit
ipfs config Swarm.RelayClient.Enabled --json false
ipfs config Swarm.Transports.Network.Relay --json false