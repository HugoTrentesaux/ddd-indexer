#!/bin/sh
set -ex

# --- rpc ---
# allow easy access through ssh tunnel on port 500x
# ssh -NL 5002:localhost:500x datapod
# ipfs --api=/ip4/127.0.0.1/tcp/500x
ipfs config API.HTTPHeaders.Access-Control-Allow-Origin --json '["http://127.0.0.1:5001","http://127.0.0.1:5002","http://127.0.0.1:5003","http://127.0.0.1:5004","http://127.0.0.1:5005"]'
ipfs config API.HTTPHeaders.Access-Control-Allow-Methods --json '["PUT", "POST"]'
