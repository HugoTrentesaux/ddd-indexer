#!/bin/sh
set -ex

# --- bootstrap ---
# remove default bootstrap nodes
ipfs bootstrap rm all
# add custom bootstrap (hugo, poka, aya)
ipfs config Bootstrap --json '[
  "/dns/datapod.coinduf.eu/tcp/4001/p2p/12D3KooWFp4JsoDo5FX8CFLtyJjaWWRZ8q3gr8uT2s9To2GYzRNA",
  "/dns/gateway.datapod.ipfs.p2p.legal/tcp/4001/p2p/12D3KooWEaBZ3JfeXJayneVdpc71iUYWzeykGxzEq4BFWpPTv5wn",
  "/dns/ipfs.asycn.io/tcp/4001/p2p/12D3KooWJnzYzJBtruXZwUQJriF1ePtDQCUQp4aNBV5FjpYVdfhc",
  "/dns/datapod.gyroi.de/tcp/4441/p2p/12D3KooWQdw3ptcSk1exiBTBDGbFzLEhoVT3zyeoU1zrpswb3qyL"
]'